<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<div class="page-content">
	<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
	<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Modal title</h4>
				</div>
				<div class="modal-body">
					 Widget settings form goes here
				</div>
				<div class="modal-footer">
					<button type="button" class="btn blue">Save changes</button>
					<button type="button" class="btn default" data-dismiss="modal">Close</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
	<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
	
	<!-- BEGIN PAGE HEADER-->
	<div class="page-bar">
		<ul class="page-breadcrumb">
			<li>
				<i class="fa fa-home"></i>
				<a href="index.html">Home</a>
				<i class="fa fa-angle-right"></i>
			</li>
			<li>
				<a href="#">Dashboard</a>
			</li>
		</ul>
	</div>
	<h3 class="page-title">
	Dashboard <small>reports & statistics</small>
	</h3>
	<!-- END PAGE HEADER-->
	<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box grey-cascade">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i>Managed Table
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-toolbar">
								<div class="row">
									<div class="col-md-6">
										<div class="btn-group">
											<a href="<?php echo base_url(); ?>admin/<?php echo $this->uri->segment(2); ?>/add">
												<button id="sample_editable_1_new" class="btn green">
													Add New <i class="fa fa-plus"></i>
												</button>
											</a>
										</div>
									</div>
									<div class="col-md-6">
										<div class="btn-group pull-right">
											<button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>
											</button>
											<ul class="dropdown-menu pull-right">
												<li>
													<a href="javascript:;">
													Print </a>
												</li>
												<li>
													<a href="javascript:;">
													Save as PDF </a>
												</li>
												<li>
													<a href="javascript:;">
													Export to Excel </a>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<table class="table table-striped table-bordered table-hover" id="sample_1">
							<thead>
							<tr>
								<th>
									No
								</th>
								<th>
									Nama Menu Admin
								</th>
								<th>
									Link
								</th>
								<th>
									Icon
								</th>
								<th>
									Urutan Ke
								</th>
								<th>
									Aktif
								</th>
								<th>
									Aksi
								</th>
								
							</tr>
							</thead>
							<tbody>
							<?php 
								$no = 1;
								foreach ($menuadmin as $q_menuadmin) {
									if($q_menuadmin->is_aktif == 1){
										$aktif = 'Ya';
									}else{
										$aktif = 'Tidak';
									}
									echo '
										<tr class="odd gradeX">
										<td>
											'.$no.'
										</td>
										<td>
											 '.$q_menuadmin->nama_menuadmin.'
										</td>
										<td>
											<a href="'.base_url().'admin/'.$q_menuadmin->link_menuadmin.'">'.base_url().'admin/'.$q_menuadmin->link_menuadmin.'</a>
										</td>
										<td>
											 '.$q_menuadmin->icon_menuadmin.'
										</td>
										<td class="center">
											'.$q_menuadmin->urutan_ke.'
										</td>
										<td class="center">
											 '.$aktif.'
										</td>
										<td>
											<a href="'.base_url().'admin/menuadmin/edit/'.$q_menuadmin->id_menuadmin.'"><button class="btn btn-primary"><i class="fa fa-pencil"></i></button></a>
											<a href="'.base_url().'admin/menuadmin/delete/'.$q_menuadmin->id_menuadmin.'"><button class="btn btn-danger"><i class="fa fa-eraser"></i></button></a>
										</td>
									</tr>
									';
								$no++;
								}
							?>
							</tbody>
							</table>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
</div>
</div>
<!-- END CONTENT -->
