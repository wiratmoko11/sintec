<?php
class model_baca extends ci_model {
	function tampil() {
		$query=$this->db->get('tutorial');
		if($query->num_rows()>0) 
		{
			return $query->result();
		}
		else
		{
			return array();
		}
	}

	function per_id($id)
	{
		$this->db->where('id_tutorial',$id);
		$query=$this->db->get('tutorial');
		return $query->result();
	}
}