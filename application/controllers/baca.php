<?php
	class baca extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->library('template_public');
			$this->load->model('model_baca');
			$this->load->helper('text');
		}
		function index(){
			$data['data']=$this->model_baca->tampil();
			$this->load->view('sintec/artikel',$data);
		}
		function selanjutnya(){
			$id=$this->uri->segment(3);
			$data['artikel'] = $this->model_baca->per_id($id);
			$this->template_public->display('sintec/selanjutnya', $data);
		}

	}